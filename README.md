# DEPRECATED

***NOTE:*** This repo has been deprecated in favor of [the open-sourced
version](https://github.com/curriculum-advantage/coconut).

***NOTE:*** Do not upgrade to `coconut` until you've reviewed all breaking changes!

# Cocos Extended

Cocos Extended is an NPM package that is meant to be used with a project that already has Cocos2d-x installed. Please see the [cocos_skeleton](https://bitbucket.org/classworks/cocos_skeleton)
repository for a quick start project. This package includes many helpers that aim to fill in the gap
of functionality that Classworks' HTML games require. You'll find that this package makes it easier
to quickly get up and running with inputs, complex text, animations, deterministic pseudo-random
data generation, and more.

## Documentation

- [Getting Started](./docs/getting-started.md)
- [Breaking Changes](./docs/breaking-changes.md)
- [Contributing](./docs/contributing.md)
