# Getting Started

## Quick Start

##### 1. Add as a dependency
`cocos-extended` is not an official NPM package (i.e. [hosted by NPM](https://www.npmjs.com/products)
, although Classworks should potentially consider subscribing to a single-user plan, allowing for an official package that is still private, in order to make this process more familiar). For this
reason, we can not simply install the latest version of the package by its name. Instead, manually
add the following line (which represents the repository url and commit hash) to your `package.json` dependency list:

`"@classworks/cocos-extended": "git+https://bitbucket.org/classworks/cocos-extended.git#f962761"`

##### 2. Install all dependencies
You will want to peform this step after installing any package, regardless of whether or not its
your first time installing or upgrading `cocos-extended` or upgrading an entirely separate package.
The reason being that there's currently an NPM bug that will uninstall privately hosted packages
(e.g. `cocos-extended`) when installing another package individually (e.g. npm i --save
somePackageName). You'll get errors about `cocos-extended` not existng, which again, will require a
full reinstall of *all* packages.

```sh
$ npm i
```

## Upgrading

##### 1. Get the version

Go to the `cocos-extended` [repository's commits, and filter by the master branch](https://bitbucket.org/classworks/cocos-extended/commits/branch/master). Select the commit hash that you want to upgrade
to. This commit is the version of the package that will be installed on your project. It's
essentially the equivalent of installing a specific version number (when installing an official NPM package). In most cases, you'll probably want the most recent commit hash from the master branch,
which represents the latest stable version.

##### 2. Replace commit hash
In your project's pacakge.json, replace the existing commit hash (i.e. `#f962761`) of the
`cocos-extended` dependency with the commit hash that you want to upgrade to.

`"@classworks/cocos-extended": "git+https://bitbucket.org/classworks/cocos-extended.git#f962761"`

## Usage

##### Importing specific exports (recommended):

```javascript
import { log } from '@classworks/cocos-extended';
log('Hello World!');
```

##### Importing the entire module:

```javascript
import cocosExtended from '@classworks/cocos-extended';
cocosExtended.log('Hello World!');
```
