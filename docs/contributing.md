# Contributing

## Changes

You should use the same git workflow that you would when contributing to any other Classworks
repository (create a feature branch, push to that branch, and submit a pull request when ready).

You can edit methods directly in the `src/` folder. You do not need to worry about any sort of
compiling/transpiling. All build steps are automatically handled by NPM when the package is
installed on a project.

## Code quality

Ensure that any changes you make pass unit tests, linting, and type checking (these checks are all
done automatically when you commit your changes locally, via commit hooks). Also be mindful of the
fact that this package is used by dozens of Classworks' HTML game repositories. Do not assume that
you are not introducing bugs simply because the package works for your specific use case.

## Testing

You can install a commit from your feature branch in the exact same way that you can [install a
commit from the master branch](./getting-started.md#Upgrading). This will allow you to test your
`cocos-extended` feature branch changes on a project, before submitting a pull request to the `cocos-extended` master branch.
