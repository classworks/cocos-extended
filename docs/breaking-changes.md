# Breaking Changes

Note that preferably, all repos would eventually be updated to the latest cocos-extended package at some point in the future, and we could remove these breaking changes notes entirely. But with each repo we learn new Cocos techniques, and our helpers improve over time, so for now, these changes are mostly necessary.

## UPGRADING TO COCONUT

- Includes all breaking changes listed in this file, in addition to:
  - `createSprite` and `createRect` argument, `pos` changed to `position`
  - `createLabel` and `createRect` argument `parent` added to the options object, with the options
    object now being the first argument.
  - `populateArray` third argument is now a boolean for whether it needs to be unique. It's fourth
    argument now accepts the `isDuplicate` function.
  - `Fraction` constructor was modified heavily, so will want to test to make sure there aren't any
    breaking changes there.

## Before commit9acb077
-On `MultiLabel`, fractions value separated was replaced from `,` to `|`

## Before commit be45a30
- On `MultiLabel`, both `EXPONENT` and `SUPER` syntax was deprecated in favor of `SUPERSCRIPT`

## Before commit 0233d4e

- On `createLable`, `dimensions` data type has been change from `cc.size()` to `[]`.

## Before commit 7ddb985

- On `createLabel`, `parent` is now optional (allowing for the label to be created without adding to the game immediately).
- On `createLabel`, `hAlign` has been changed to `horizontalAlign`.
- On `createLabel`, `vAlign` has been changed to `verticalAlign`.
- On `createLabel`, `enableScale` and `scaleFactor` have been deprecated and removed.
- On `createLabel`, `enableStroke` has been deprecated and removed, and `strokeSize` default has been set to 0 (rather than 1). To enable stroke, simply pass in a `strokeSize` greater than `0`.

## Before commit 972ee71

- `addClickHandler` on `MultiLabel` have been removed. Used the `clickHandler` function option instead.
- `setLabelFontDefinition` on `MultiLabel` have been removed. Use `fontWeight, fontStyle` option instead.
  - To style a specific label on `MultiLabel`, use the `getLabels` method and iterate over the list.
- `verticalAlignment` option on `MultiLabel` have been removed. Use `MultiLabel.setPositionY()` instead.
- `justify` option on `MultiLabel` have been removed. Use the `horizontalAlignment` option instead.

## Before commit 94ab01f

- `bold` option on `createLabel` has been deprecated. Use `fontWeight` instead (pass in 700 for bold).
- `pos` option on `createLabel` has been changed to `position`, with more explicit naming agreed on by team

## Before commit 9b8476d

- If game repo is using the old retina strategy (e.g. `cc.view.enableRetina(window.IOS)`), then `createLabel` now requires the `enableScale` option (e.g. `createLabel({ enableScale: true })`). The new strategy is to allow retina on all capable devices, therefore making custom font scaling irrelevant.

- `prandoShuffle()` no longer guarantees by default that EVERY element from the returned array will
be unique when compared to the element of the same index from the array being passed in. You must
now explicitly pass in `true` as the third argument to enable this option.
