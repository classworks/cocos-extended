/*
 * Copyright (c)  Curriculum Advantage, Inc. 2018
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import log from '../log';

const onlyDebug = (func) => {
  if (window.DEBUG) {
    func();
    if (Object.keys(window.debugInfo).length) log('Debug info: ', window.debugInfo);
  }
};

const ensureInfo = () => {
  onlyDebug(() => {
    if (!window.debugInfo) window.debugInfo = {};
  });
};

export const markDebugLoaded = () => {
  ensureInfo();
  onlyDebug(() => {
    window.debugInfo.loaded = true;
  });
};

export const updateDebugScore = (scoreArray) => {
  ensureInfo();
  onlyDebug(() => {
    window.debugInfo.score = scoreArray;
  });
};
