/*
 * Copyright (c)  Curriculum Advantage, Inc. 2018
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

const getCachedFrame = name => cc.spriteFrameCache.getSpriteFrame(name);

export default getCachedFrame;
