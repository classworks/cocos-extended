import Prando from 'prando';
import log from '../log';

import getParameterByName from '../getParameterByName';

/**
 * Returns a pseudorandom number generator, created with Prando, using the `p2` query option (set in the url) as the seed.
 *
 * @see {@link https://en.wikipedia.org/wiki/Pseudorandom_number_generator|Wikipedia Explanation}
 * @see {@link https://github.com/zeh/prando|Prando}
 *
 * @example
 *
 * // If URL is http://127.0.0.1:8000/?p2=123, the following invocation will return a Prando instance using the seed 123.
 * createPseudorandomNumberGenerator();
 */
const createPseudorandomNumberGenerator = () => {
  let seed = getParameterByName('param2') || getParameterByName('p2');
  if (!seed || seed === '0') seed = `${Date.now()}`;
  log('Seed: ', seed);
  return new Prando(seed);
};

export default createPseudorandomNumberGenerator;
