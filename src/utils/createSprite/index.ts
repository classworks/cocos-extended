/*
 * Copyright (c)  Curriculum Advantage, Inc. 2018
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

const resourcePath = (resource, isImage) => {
  const shouldInitiateWithSprite = resource !== null && !isImage;
  return shouldInitiateWithSprite ? `#${resource}` : resource;
};

/**
 * Creates a sprite with either an image (using isImage option) or a sprite frame.
 */
const createSprite = ({
  aligned = false,
  parent = null,
  resource = null,
  isImage = false,
  pos = [300, 300],
  anchor = [0.5, 0.5],
  zOrder = 0,
  scale = [window.g_scaleRatio || 1, window.g_scaleRatio || 1],
} = {}) => {
  const sprite = new cc.Sprite(resourcePath(resource, isImage));

  if (parent) parent.addChild(sprite, zOrder);

  const anchorPoints = aligned ? [0, 1] : anchor;
  sprite.setAnchorPoint(...anchorPoints);

  const positions = aligned ? [0, 720] : pos;
  sprite.setPosition(...positions);

  sprite.setScale(...scale);

  return sprite;
};

export default createSprite;
