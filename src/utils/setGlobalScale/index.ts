/*
 * Copyright (c)  Curriculum Advantage, Inc. 2018
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

const setGlobalScale = (...sprites) => {
  sprites.forEach(sprite => sprite.setScale(window.g_scaleRatio, window.g_scaleRatio));
};

export default setGlobalScale;
