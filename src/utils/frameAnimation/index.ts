/*
 * Copyright (c)  Curriculum Advantage, Inc. 2018
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

import { isPlainObject } from 'lodash';

import getCachedFrame from '../getCachedFrame';

/**
 * Formats the file number to include preceding zeros
 */
const fileNum = (num, placeCount) => {
  const numZerosToAdd = placeCount - `${num}`.length;
  if (numZerosToAdd > 0) {
    const zeros = new Array(numZerosToAdd).fill(0).join('');
    return `${zeros}${num}`;
  }
  return num;
};

/**
 * Creates a cc.Animation object which can be used as an argument to a cc.Animate action.
 *
 * Keep in mind, although this helper includes many convenience options, optimally, all sprites
 * should have the same static path, be in a sequential order starting with 1, and not skip any
 * frame numbers in between — providing for a cleaner codebase overall.
 *
 * An array including information about the sprites being animated is required. Optionally, you can
 * pass multiple frame set arrays, each as separate arguments. This can be useful when an animation
 * includes sprites that are spread across multiple paths. This array/arrays can be passed
 * starting with either the first or second argument. Each array includes:
 *   - last file number of frame set (not including preceding zeros)
 *   - static portion of file path, up to, but not including the file number and extension
 *   - place count of the file number (optional, defaults to 3) (example: 001 has place count of 3)
 *   - starting file number of frame set (optional, defaults to 1)
 *   - file numbers that are missing and should be skipped (optional)
 *
 * An optional options object that controls the animation can be passed as the first argument,
 * containing a custom `delay` and/or `loops` property, which are then passed to the cc.Animation
 * object.
 *
 * Examples:
 *
 * const basicAnimation = frameAnimation([8, 'path-to-file']);
 *
 * const customAnimation = frameAnimation(
 *   { delay: 4, loops: 2 },
 *   [8, 'path-to-file'],
 *   [44, 'path-to-file', 5, 4, [102, 55, 87]],
 *   [23, 'path-to-file'],
 * );
 *
 * TODO: We could require that frameNumEnd include the preceding zeros and then automatically grab
 * fileNumPlaceCount, but this would be a breaking change.
 */

const frameAnimation = (firstArg, ...rest) => {
  const optionsSupplied = isPlainObject(firstArg);
  const customSettings = optionsSupplied ? firstArg : {};
  const { delay, loops } = { delay: 0.111, loops: 1, ...customSettings };
  const frameSets = optionsSupplied ? rest : [firstArg, ...rest];

  const frames = frameSets.reduce(
    (
      acc,
      [
        frameNumEnd,
        staticFilePath,
        fileNumPlaceCount = 3,
        frameNumStart = 1,
        skippedFrameNums = [],
      ],
    ) => {
      for (let i = frameNumStart; i <= frameNumEnd; i += 1) {
        if (!skippedFrameNums.includes(i)) {
          const filename = `${staticFilePath}${fileNum(i, fileNumPlaceCount)}.png`;
          acc.push(getCachedFrame(filename));
        }
      }

      return acc;
    },
    [],
  );

  return new cc.Animation(frames, delay, loops);
};

export default frameAnimation;
