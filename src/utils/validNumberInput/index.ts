/*
 * Copyright (c)  Curriculum Advantage, Inc. 2018
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

// Allows only numbers to be typed by the user. Will not update the text input otherwise.
const validNumberInput = str => str.split('').reduce((acc, char) => {
  const charToNum = +char;
  const isNotSpace = char !== ' ';
  const canConvertToNum = typeof charToNum === 'number' && !Number.isNaN(charToNum);
  const isValid = isNotSpace && canConvertToNum;
  const validString = isValid ? `${acc}${char}` : acc;
  return validString.trim();
}, '');

export default validNumberInput;
