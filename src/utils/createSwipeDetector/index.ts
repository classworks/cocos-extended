/*
 * Copyright (c)  Curriculum Advantage, Inc. 2018
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

// NOTE: invoke function factory on touch began event
const createSwipeDetector = () => {
  const startTouchTime = Date.now();

  // NOTE: invoke closure on touch end event
  return (touch) => {
    const startPoint = touch._startPoint; // eslint-disable-line no-underscore-dangle
    const endPoint = touch._point; // eslint-disable-line no-underscore-dangle

    const deltaX = endPoint.x - startPoint.x;
    const deltaY = endPoint.y - startPoint.y;

    const absX = Math.abs(deltaX);
    const absY = Math.abs(deltaY);

    const time = Date.now() - startTouchTime;
    const velocity = Math.sqrt((absX * absX) + (absY * absY)) / time;
    const isFlick = velocity > 0.5;

    let up = false;
    let down = false;
    let left = false;
    let right = false;

    if (isFlick) {
      const angleFactor = 5;
      const distanceFactor = 40;
      const isLeftRight = (absX > distanceFactor) && (absX > (absY / angleFactor));
      const isUpDown = (absY > distanceFactor) && (absY > (absX / angleFactor));

      if (isUpDown) {
        if (deltaY > 0) up = true;
        else down = true;
      } else if (isLeftRight) {
        if (deltaX > 0) right = true;
        else left = true;
      }
    }

    return {
      up, down, left, right,
    };
  };
};

export default createSwipeDetector;
