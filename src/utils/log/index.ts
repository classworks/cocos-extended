/*
 * Copyright (c)  Curriculum Advantage, Inc. 2018
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

const log = (...args) => {
  if (window.DEBUG) console.log(...args);
};

export default log;
